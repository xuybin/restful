package cn.mydsl.restful

/**
 * Created by xuybin on 17-2-14.
 */
class OAuth2ServerSettings(
        val authorizeUrl: String="/authorize",
        val accessTokenUrl: String="/token",
        val clientId: String,
        val clientSecret: String,
        val defaultScopes: List<String> = emptyList(),
        val accessTokenRequiresBasicAuth: Boolean = false
)