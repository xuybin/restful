
# 通用的资源服务器
满足restful规范的通用API,以HTTP协议受控的CRUD数据库数据

## 库,表,字段,名称和字段筛选值规则
- 库名,表名,字段名三者 不能包含符号\` 空格 符号. 和 符号[ 符号]
- 从URL上提取当前表名后，组成key( \`默认库名\`.\`当前表名\`或 \`当前表名\`) 查找服务端保存的表信息，以服务器的表信息，拼接sql
- 正则时提取 字段名时,排除 符号\`空格符号[  符号],以提取值组成key( 服务器当前表信息中的库名和表名.\`字段名\`或 \`默认库名\`.\`字段名\` 或 \`字段名\` )，查询服务端保存的字段信息,以服务器的字段信息，拼接sql
- 从URL上提取字段筛选值后,首先判断第一个和最后一个字符是单引号,是,则提取中的数据,替换内部的每个单引号为双单引号,最后用单引号前后包裹,以此拼接sql

## 用户相关权限规则
解决 某用户,对某表,进行某操作时,需要满足的条件
### 操作的主体
- 当前登录用户(利用存档数据包含的信息检查,如关联的uid,oid等)
- 匿名用户(利用存档数据包含的信息检查,如关联的uid,oid等)

### 需要检查的操作
- 插入(如：审核相关的字段不能由前台插入,无需查考范围)
- 查询(如：普通用户进货价格字段不能查询到前台)
- 更改(如：管理员才能更新审核相关字段)
- 删除(无需参考主体明细)

### 被操作的主体
- 表名(不带db名和\`)

### 不能被操作的主体明细
- 字段名(不带db名,table名和\`)
- 空(表的所有字段都应用当前操作)

### 操作的范围
- **库.表.身份字段=[身份数据]**: 身份字段userID|orgID|jobID|scopeID等,身份数据即当前用户对应的身份字段的数据
- **预设值 =[身份数据]**:
- 空(表的所有数据条目都可应用当前操作)

## CRUD规则
### Retrieve查询规则
- **值相等**: s[eq[field1|field2]]=value|value
- **值不相等**: s[!eq[field1|field2]=value1|value
- **值模糊相等**: s[lk[field1|field]]=value|value
- **值在某范围**: s[rng[field1|field2]]=value,value|value,value
- **值在集合内**: s[in[field1|field2]]=value,value,value|value,value
- **值在不在集合内**: s[!in[field1|field2]]=value,value|value,value,value
- **两个字段比较(可用于手动关联表)**: cmp[field1|(gt,lt,gteq,lteq,eq,!eq)|field2]
- **手动指定字段**: flt[incl[field1|field2]]
- **手动排除字段**: flt[excl[field1|field2]]

### 排序规则
- **升序排序**: ord[asc[field1|field2|field3]]
- **降序排序**: ord[desc[field|field2|field3]]

### 占位符号
- **格式化**: fmt[([0]\_or\_[1])\_and\_[2])\_order\_by\_[4],[3]]

### 分页参数
- **页码**: page=n
- **页大小**: size=n

### 外键查询的支持
- **返回关联表的字段**: joint=tablename
- **关联表字段筛选**:field=table.fieldName--> s[eq[table.fieldName]]=value

### 请求例子
- **GETURL**:http://127.0.0.1:8080/auth_db.t1/?s[rng[fn]]=1,&ord[desc[fn]]&ord[asc[fs]]&fmt[[0]_order_by_[2],[1]]&page=1&size=2

- **拼接的SQL**:SELECT fn,fs,UNIX_TIMESTAMP(ft) as ft FROM \`auth_db\`.\`t1\` WHERE \`auth_db\`.\`t1\`.\`fn\` >= 1  order by \`auth_db\`.\`t1\`.\`fs\` ASC,\`auth_db\`.\`t1\`.\`fn\` DESC LIMIT 2 OFFSET 0

- **GETURL**:http://127.0.0.1:8080/auth_db.t2/?joint=auth_db.t1|auth_db.t3&s[eq[auth_db.t3.id]]=111

- **拼接的SQL**:SELECT \`auth_db\`.\`t2\`.\`id\`,\`auth_db\`.\`t2\`.\`t1Fkid\`,\`auth_db\`.\`t2\`.\`fn\`,\`auth_db\`.\`t1\`.\`id\` AS \`t1Id\`,\`auth_db\`.\`t1\`.\`fn\` AS \`t1Fn\`,UNIX_TIMESTAMP(\`auth_db\`.\`t1\`.\`ft\`) AS \`ft\`,\`auth_db\`.\`t3\`.\`id\` AS \`t3Id\`,\`auth_db\`.\`t3\`.\`t2Fkid\`,\`auth_db\`.\`t3\`.\`fn\` AS \`t3Fn\`,\`auth_db\`.\`t3\`.\`fs\`,UNIX_TIMESTAMP(\`auth_db\`.\`t3\`.\`ft\`) AS \`t3Ft\` FROM \`auth_db\`.\`t2\`,\`auth_db\`.\`t1\`,\`auth_db\`.\`t3\` WHERE \`auth_db\`.\`t2\`.\`t1Fkid\`=\`auth_db\`.\`t1\`.\`id\` AND \`auth_db\`.\`t2\`.\`id\`=\`auth_db\`.\`t3\`.\`t2Fkid\`  AND ( \`auth_db\`.\`t3\`.\`id\` = 111    ) 

## TO DO
### Create
- **单条创建**
- **批量创建**:...

### Update
- **单条更新**:...
- **批量更新**

### Delete
- **单条/批量删除**:...
- **关联子表删除**:...


## 参考资料

- 走向无后端的系统开发实践：CRUD自动化与强约定的REST接口http://mp.weixin.qq.com/s/snxi3rI9O1ykO6NHXk0ebg
- 前端和后端的区别，与后端统一化的理论基础 思考
http://www.jianshu.com/p/e0b9a56ab9fd

