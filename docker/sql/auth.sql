CREATE DATABASE IF NOT EXISTS `auth_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `auth_db`;

CREATE DATABASE IF NOT EXISTS `auth_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `auth_db`;

###########################认证元数据###########################
CREATE TABLE `AuthMtadata` (
    `tableName` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '被操作的主体表名',
    `keyword` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '需要检查的操作关键字SELECT|INSERT|UPDATE|DELETE',
    `disableField` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '不能操作的字段名,以|分割',
    `range` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作的范围,涉及的字段，必须带上数据库名.表名',

    `operator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'ouid-001' COMMENT '操作者',
    `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY  (`tableName`,`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `AuthMtadata`(`tableName`,`keyword`,`disableField`,`range`) VALUES("User","SELECT","password","`auth_db`.`User`.`id`=[uid] OR 'admin' in ([jib])");
INSERT INTO `AuthMtadata`(`tableName`,`keyword`,`disableField`,`range`) VALUES("Emp","SELECT","password","`auth_db`.`Emp`.`id`=[eid] OR 'admin' in ([jib])");
INSERT INTO `AuthMtadata`(`tableName`,`keyword`,`disableField`,`range`) VALUES("Org","SELECT','password","`auth_db`.`Org`.`id`=[oid] OR 'admin' in ([jib])");

###########################认证元数据###########################


/****************************所有组织可以使用所有资源*****************************
先有资源（系统）,系统上有工作内容，工作需要个人承担或企业组织承但，员工代表企业完成工作
所有组织可以使用所有资源--->无需特殊标识某个组织拥有某资源的访问权限，token内无需追加aud
免费使用，但业务上有限制--->新组织需要审核或只能使用免费功能，在更高的业务层，通过接口调用，处理此类问题
不同的资源有不同的权限范围--->每个组织员工或个人token内需追加Scope（或role 或Job）
****************************/
###########################工作定义###########################
CREATE TABLE `Job` (
    `id` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '唯一标识',
    `jobName` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '工作名称（中文名称）',
    `jobDsc` text COLLATE utf8mb4_unicode_ci NULL COMMENT '工作说明',
    `jobScope` text COLLATE utf8mb4_unicode_ci NULL COMMENT '工作权限生效（适用）的范围',

    `operator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'ouid-001' COMMENT '操作者',
    `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',

    PRIMARY KEY (`id`),
    UNIQUE  KEY `unique_client_resource` (`jobName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='组织的员工';

INSERT INTO `Job`(`id`,`jobName`) VALUES('jid-001','测试job1');
INSERT INTO `Job`(`id`,`jobName`) VALUES('jid-002','测试job2');
###########################工作定义###########################


###########################个人与工作内容关系###########################
CREATE TABLE `UserJob` (
    `UserId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户外键',
    `JobId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '工作内容外键',

    `operator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'ouid-001' COMMENT '操作者',
    `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     PRIMARY KEY (`UserId`,`JobId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='个人与工作内容关系';
INSERT INTO `UserJob`(`UserId`,`JobId`) VALUES('uid-001','jid-001');

CREATE TABLE `User` (
    `id` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '唯一标识',
    `mobileNo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户手机号码--登录方式一的登录名',
    `userName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户名称--登录方式二的登录名',
    `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码--验证码--登录凭证二',

    `nickName` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '昵称',
    `headImg` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户头像',

    `realAuth` tinyint(4) DEFAULT '0' COMMENT '是否真实认证',
    `idCard` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '身份证--登录方式三的登录名',
    `idCardImg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '身份证正反面照片',
    `realName` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '真实姓名',
    `sex` tinyint(2) DEFAULT '0' COMMENT '性别(1,男,2女)',
    `addressCode` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址代码',
    `addressDetail` text COLLATE utf8mb4_unicode_ci COMMENT '地址详情',

    `OrgId` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '默认组织ID',

    `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
    `operator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'ouid-001' COMMENT '操作者',
    `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',

    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_mobileNo` (`mobileNo`),
    UNIQUE KEY `unique_idCard` (`idCard`),
    UNIQUE KEY `unique_userName` (`userName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户身份信息';

INSERT INTO `User`(`id`,`userName`,`password`) VALUES('uid-001','system','system123');
INSERT INTO `User`(`id`,`userName`,`password`,`OrgId`) VALUES('uid-002','user','password123','oid-001');
###########################个人与工作内容关系###########################


###########################员工与工作内容关系###########################
CREATE TABLE `EmpJob` (
    `EmpId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '员工外键',
    `JobId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '工作内容外键',

    `operator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'ouid-001' COMMENT '操作者',
     `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     PRIMARY KEY (`EmpId`,`JobId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='员工与工作内容关系';
INSERT INTO `EmpJob`(`EmpId`,`JobId`) VALUES('eid-001','jid-001');
INSERT INTO `EmpJob`(`EmpId`,`JobId`) VALUES('eid-001','jid-002');

CREATE TABLE `Emp` (
    `id` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '唯一标识',
    `OrgId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '组织ID',
    `UserId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户标识',

    `empDsc` text COLLATE utf8mb4_unicode_ci COMMENT '员工描述',
    `hireDate` date DEFAULT NULL COMMENT '入职日期',

    `lastAccessTokenJti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '最新登录的token标识',
    `lastRefreshToken` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '最新token对应的刷新token',
    `lastLoginDate` timestamp NULL DEFAULT NULL COMMENT '最新登录的时间戳',
    `totalLoginCount` int(11) DEFAULT NULL COMMENT '总登录次数',

    `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
    `operator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'ouid-001' COMMENT '操作者',
    `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',

    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_org_user` (`OrgId`,`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='组织的员工';

INSERT INTO `Emp`(`id`,`OrgId`,`UserId`) VALUES('eid-001','oid-001','uid-002');
###########################员工与工作内容关系###########################


###########################组织与员工关系###########################
CREATE TABLE `Org` (
    `id` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '唯一标识|存在相同的ClientId',
    `UserId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '企业法人',
    `OrgClassId` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '组织分类',

    `secret` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '凭证',
    `grantType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '授权模式',
    `redirectUri` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '客户端的域名',

    `orgName` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '组织名称',
    `orgImages` text COLLATE utf8mb4_unicode_ci COMMENT '组织图片',

    `orgDsc` text COLLATE utf8mb4_unicode_ci COMMENT '组织（客服端）描述',

    `addressCode` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址代码',
    `addressDetail` text COLLATE utf8mb4_unicode_ci COMMENT '地址详情',


    `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
    `operator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'ouid-001' COMMENT '操作者',
    `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='组织';

INSERT INTO `Org`(`id`,`secret`,`grantType`,`redirectUri`,`OrgClassId`,`orgName`,`orgImages`,`UserId`,`orgDsc`,`addressCode`,`addressDetail`) values ('personal','personalSecretKey123',NULL,NULL,'org_other','所有个人用户的归属组织',NULL,'ouid-001','所有个人用户的归属组织','520000000000','贵州省');
INSERT INTO `Org`(`id`,`secret`,`grantType`,`redirectUri`,`OrgClassId`,`orgName`,`orgImages`,`UserId`,`orgDsc`,`addressCode`,`addressDetail`) values ('oid-001','orgSecretKey123',NULL,NULL,'org_farm','测试组织',NULL,'ouid-001','测试组织','520000000000','贵州省');

CREATE TABLE `OrgClass` (
    `id` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '唯一标识',
    `orgClassName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '组织分类名称',
    `orgClassDsc` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '组织分类说明',

    `operator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'ouid-001' COMMENT '操作者',
    `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='组织类型';

INSERT INTO `OrgClass`(`id`,`orgClassName`,`orgClassDsc`)values ('org_farm','农场','农场组织'),('org_other','其他未分类组织','其他未分类组织');
###########################组织与员工关系###########################